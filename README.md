# APRLmvr

This is a package for multivariate regression (MVR) for use with infrared spectra of laboratory or atmospheric samples. Currently implemented for PLS1 and PLS2 using the `pls` package, but development version also uses other types of multivariate regression (e.g., sparse PLS and elastic net).

Key inputs:
* response values
* spectra matrix (+ blank spectra matrix)
* sample list (+ blank sample list)
* PLS parameters

The package lightly wraps the `mvr` function in the pls package. The
main objective (value-added proposition) of the package is to facilitate:

* splitting samples between calibration and test sets,
* addition of blank samples,
* preparation of spectra and response variables when blanks of samples
below MDL are present,
* extract information from R objects into formatted tables, and
* apply suite of evaluation metrics

The goal is to distance the user from the task of implementing these
conventions, with examples demonstrating how the functionality can be
accessed by specification of key parameters through JSON (JavaScript
Object Notation) files.

Examples input files and scripts that call the relevant functions are
provided, which the user may freely modify.


## Installation Instructions

First, please see [installation instructions for APRLspec](https://gitlab.com/aprl/APRLspec), on which APRLmvr depends.

Install for R (replace `{username}` and `{password}` with account for which access to private repository is granted):

```{r}
> devtools::install_git('https://{username}:{password}@gitlab.com/aprl/APRLmvr.git')
```

After installation, get a copy of example files (templates). To copy them into a new folder called "mvr" in your working directory:

```{sh}
$ cd /path/to/workingdir/
$ R -e "APRLspec::CopyPkgFiles('APRLmvr', 'example_files', 'mvr')"
$ cd mvr/
```
To copy them into an existing working directory ("." as third argument) - e.g., the current one where you have used baseline correction scripts - you can optionally pass a prefix as the fourth argument to the `CopyPkgFiles` function:

```{sh}
$ cd /path/to/workingdir/
$ R -e "APRLspec::CopyPkgFiles('APRLmvr', 'example_files', '.', 'mvr')"
```
This will add a prefix to all the example files in this directory to prevent naming conflicts with files from other packages.

To uninstall, run the command in R:

```{sh}
> remove.packages("APRLmvr")
```

Generally, the way we have run it we have had separate directories:

* spectra/: spectra files
* responses/: response tables
* cases/: CSV or JSON files that contain different partitioning/ordering of sample and blank files between calibration and test sets.

## Scripts

Note that paths will be relative to the directory from which you are
running the script. For instance, it is possible to run

```
$ cd /path/to/workingdir/
$ Rscript /path/to/scriptdir/script.R runfolder/userinput.json
```

The files in `userinput.json` must be defined relative to
`workingdir`. If files to be read from or written to are in
`runfolder`, prefix the name with `./` (this is a not a general R
convention but one adopted for this package).

### Generate casefiles

"generate\_casefiles.R" generates several instances of partitioning of samples between
calibration and test sets:

```
$ Rscript generate_casefiles.R testcase/userinput.json
```

User inputs:

* `samples`
* (optional) `N`
* (optional) `responsefile`
* (optional) `variables`
* (optional) `prefix`

Outputs when `responsefile` and `variables` are not defined:

* case file where every `N` samples are taken and placed in the test
  set, and the rest in calibration.

Outputs when `responsefile` and `variables` are defined:

* case file by site-date ("base case")
* case file by concentration:
    * uniform
    * non-uniform (A, B, C, ...)


### Fit regression

"main\_fits.R" builds the calibration and test sets by removing MDL,
inserting blanks, and optionally reordering the calibration samples
according to concentration. Then the model is fit to the calibration
samples and predictions on test set samples are made for all number of
components fitted.

```
$ Rscript main_fits.R testcase/userinput.json
```

User inputs for model fitting:

* `responsefile`
* `variables`
* `param`
* `casefile`
* `specfile`
* (optional) `blankscasefile`
* (optional) `blanksspecfile`
* (optional) `plstype`
* (optional) `reordercalib`
* (optional) `mdlfile`
* (optional) `wavenumfile`

User inputs for output file names:

* (optional) `prefix`
* (optional) `fitsfile`
* (optional) `rmsepfile`
* (optional) `predfile`


Outputs:

* File saved to `fitsfile`
* File saved to `rmsepfile`
* File saved to `predfile`

Description:

Blanks are inserted into samples every *n*-th interval, where
n=n<sub>blanks</sub>/n<sub>samples</sub>.

Since `rmsepfile` contains RMSECV (RMSE of cross validation) information
derived from the fitsfile when CV is used, it seems redundant to
save this structure alongside fits. However, when no cross validation
is used, rmsep contains values for RMSEV (RMSE of validation) which is
estimated against the test set samples (including blanks) modified in
this script. eval\_fits.R will not reconstruct the test set samples; it
only uses information contained in the rmsepfile to generate a
set of predictions.


### Evaluation of fits

"eval\_fits.R" provides summary statistics and figure for a selected model.

```
$ Rscript eval_fits.R testcase/userinput.json
```
User inputs for model selection:

* (optional) `variables`
* (optional) `metric`
* (optional) `ncomp`

User inputs for input file names:

* (optional) `prefix`
* (optional) `fitsfile`
* (optional) `rmsepfile`
* (optional) `predfile`

User inputs for output file names or figure units:

* (optional) `fitsplot`
* (optional) `predtable`
* (optional) `statfile`
* (optional) `prefix`
* (optional) `units`

Outputs:

* File saved to `fitsplot`
* File saved to `predtable`
* File saved to `statsfile`

### Prediction on new spectra

"eval\_pred\_aux.R" takes the fitted model and predicts concentrations for new
spectra for a selected number of components.

```
$ Rscript eval_pred_aux.R testcase/aux.json
```

User inputs for model prediction/evaluation:

* `specfile`: new data
* (optional) `wavenumfile` (wavenumbers corresponding to model in `fitsfile`)
* (optional) `variables` (subset of original variables used for model fitting)
* (optional) `categoryvar`

User inputs for input file names:

* (optional) `fitsfile`
* (optional) `rmsepfile`

User inputs for output file names:

* (optional) `fitsplot`
* (optional) `predtable`
* (optional) `statfile`
* (optional) `prefix` (`"aux"` by default)

Outputs (if `repsonsefile` not specified):

* File saved to `predtable`

Outputs (if `repsonsefile` specified):

* (optional) `fitsplot`
* (optional) `predtable`
* (optional) `statfile`

## Input files

### Description of JSON file arguments

For model fitting or selection:

* `variables`: [vector of strings] variables corresponding to columns in
`responsefile` to be used for calibration.
* `categoryvar`: [string] name of (categorical) variable in
  `responsefile` used for grouping statistics (used by
  eval\_pred\_aux.R only).
* `plstype`: [string] either `"PLS1"` or `"PLS2"`. Default is `"PLS1"`
  if no value if undefined.
* `param`: [list] parameters to be passed to R's `pls::mvr` function
* `reordercalib`: [`true`/`false`] reorder calibration samples according to concentration (for interleaved cross validation).
* `metric`: [string] name of metric for selecting number of components. Default is `"RMSE"`; [not implemented in this version:] permissible values are `"C1"` and `"C2"` (See Kalivas and Palmer, 2014).
* `ncomp`: [integer] a named vector for each variable. Default is NA
  (which corresponds to minimum RMSE criterion). When specified,
  overrides `metric` specification.

File names:

* `responsefile` [string] path to file containing values
  of response variable(s). Required format: comma-separated (.csv) or
  tab-delimited (.txt). Required columns: (sample, {variables});
  optionally category column.
* `casefile`: [string] file containing calibration and test set samples.
* `blankscasefile`: [string/list] path to .json file or folder.
* `specfile`: [string] path to .rda file containing spectra matrix with "wavenum".
* `blanksspecfile`: [string] path to .rda file.
* `mdlfile`: [string] path to .json file containing MDLs.
* `wavenumfile`: [string] name of file containing indices for
  wavenumber selection (this file should contain integers or list of
  integers corresponding to each variable). Default is `TRUE` (use all wavenumbers). Nominally generated by other scripts (e.g., sparse PLS or elastic net regularization).
* `fitsfile`: [string] name of file to read from/to. Default is `"fits.rda"`.
* `rmsepfile`: [string] name of file to read from/to. Default is `"rmsep.rda"`.
* `predfile`: [string] name of file to read from/to. Default is `"pred.rda"`.
* `prefix`: [string] prefix to output of evaluation files (does not
  affect naming of `fitsfile`, `rmsepfile`, or `predfile`). Default is
  `"aux"` for eval\_pred\_aux.R. .
* `fitsfile`: [string] name of file to read from/to. Default is `"fits.rda"`.
* `rmsepfile`: [string] name of file to read from/to. Default is `"rmsep.rda"`.
* `predfile`: [string] name of file to read from/to. Default is
`"pred.rda"`.
* `predtable`: [string] Default is `"prediction_table.txt"`.
* `statsfile`: [string] Default is `"stats.txt"`.
* `fitsplot`: [string] Default is `"fitsplot.pdf"`.
* `units`: [string] name of units: micrograms, micromoles,
etc. Default is `"micrograms"`.
* `samplefile`: [string] name of file containing list of samples

### Case file

Can be specified as one of the following:

* a JSON file with named elements "calibration" and "test"
* a folder containing "calibration.txt" and "test.txt".

When cross validation is used, the calibration set is used for both "training" and "validation"; the test set is completely separate from the model building process. When no validation method is specified, the test set is used for "validation".

### Spectra matrix

Can be specified as one of the following:

* An RDS or RDA file containing a matrix {samples} x {wavenumbers} with attribute "wavenum".
* A CSV file with wavenumbers in the first column and each spectra in
  subsequent columns (spectra names will be taken from the column
  labels). The name of the first (wavenumber) column is not used.

### Response table

A table containing:

* a column labeled "sample"
* (optional) "SiteCode" and "Date" (in sortable form) columns
* additional columns of concentrations (`variables` defined in user
  inputs should match these column names) or categorical columns

## Test cases


### "testcase\_TOROC": TOR OC prediction using ambient samples

Input files provided:

* userinputs.json
* userinputs_aux.json

Step-by-step:

1. Generate casefiles:

    ```
    $ Rscript generate_casefiles.R casefiles/userinput.json
    $ Rscript generate_casefiles.R casefiles_blanks/userinput.json
    ```

2. Build the model:

    ```
    $ Rscript main_fits.R testcase_TOROC/userinput.json
    ```

3. Provide evaluation:

    ```
    $ Rscript eval_fits.R testcase_TOROC/userinput.json
    ```

4. Make predictions for new samples:

    ```
    $ Rscript eval_pred_fits.R testcase_TOROC/userinput_aux.json
    ```
    In this case the same spectra and response files as for the fitting
are provided, but demonstrates the use of categorical variables for
obtaining group-specific information.

### "testcase\_FG": functional group calibration using laboratory standards

Input files provided:

* userinputs.json

Step-by-step:

1. Build the model:

    ```
    $ Rscript main_fits.R testcase_FG/userinput.json
    ```

2. Provide evaluation:

    ```
    $ Rscript eval_fits.R testcase_FG/userinput.json
    ```
