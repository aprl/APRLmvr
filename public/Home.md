APRLmvr
===

You have reached the repository for APRLmvr. This R package consolidates functionality for multivariate regression and calibration using aerosol FT-IR spectra described in the following manuscripts:

* Dillner, A. M. and Takahama, S.: Predicting ambient aerosol thermal-optical reflectance (TOR) measurements from infrared spectra: organic carbon, *Atmos. Meas. Tech.*, 8, 1097-1109, doi:[10.5194/amt-8-1097-2015](http://dx.doi.org/10.5194/amt-8-1097-2015), 2015.
* Dillner, A. M. and Takahama, S.: Predicting ambient aerosol thermal–optical reflectance measurements from infrared spectra: elemental carbon, *Atmos. Meas. Tech.*, 8, 4013-4023, doi:[10.5194/amt-8-4013-2015](http://dx.doi.org/10.5194/amt-8-4013-2015), 2015.
* Reggente, M., Dillner, A. M., and Takahama, S.: Predicting ambient aerosol thermal–optical reflectance (TOR) measurements from infrared spectra: extending the predictions to different years and different sites, *Atmos. Meas. Tech.*, 9, 441-454, doi:[10.5194/amt-9-441-2016](http://dx.doi.org/10.5194/amt-9-441-2016), 2016.

Additional operations described by

* Takahama, S., and Dillner, A. M.: Model selection for partial least squares calibration and implications for analysis of atmospheric organic aerosol samples with mid-infrared spectroscopy. *J. Chemometrics*, 29, 659–668, doi:[10.1002/cem.2761](http://dx.doi.org/10.1002/cem.2761), 2015.
* Takahama, S., Ruggeri, G., and Dillner, A. M.: Analysis of functional groups in atmospheric aerosols by infrared spectroscopy: sparse methods for statistical selection of relevant absorption bands, *Atmos. Meas. Tech.*, 9, 3429-3454, doi:[10.5194/amt-9-3429-2016](http://dx.doi.org/10.5194/amt-9-441-2016), 2016

will be incorporated in future editions.

## Main features

* Staging calibrations using laboratory or ambient infrared spectra and reference concentrations, with optional specification of blank samples and limits.
* Full range of evaluation statistics and graphics for model selection and evaluation.

The core regression functionality in this version is enabled by the `pls` package in R.

## Installation

The repository mirror is [https://gitlab.com/aprl/APRLmvr](https://gitlab.com/aprl/APRLmvr) (visible with credentials). Please contact the package author for access.
